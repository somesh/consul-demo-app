package config

import (
	"time"

	"gopkg.in/tokopedia/logging.v1/tracer"
)

var CF *Config

type Config struct {
	Server         ServerConfig
	Features       map[string]*Feature
	Tracer         tracer.Config
	Timeout        time.Duration
	Consul         ConsulConfig
	Grace          GraceCfg
	CircuitBreaker CircuitBreakerCfg
}

type ServerConfig struct {
	Host         string
	Port         int
	Timeout      time.Duration
	TemplatePath string
}

type Feature struct {
	Enabled bool
}

type ConsulConfig struct {
	Address string
	Port    int
	Token   string
}

type GraceCfg struct {
	Timeout          string
	HTTPReadTimeout  string
	HTTPWriteTimeout string
}

type CircuitBreakerCfg struct {
	Enabled             bool
	GamificationEnabled bool
	ErrorThreshold      int
	SuccessThreshold    int
	TimeoutSec          int
}
