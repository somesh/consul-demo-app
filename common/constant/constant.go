package constant

import (
	"errors"
)

const (

	// Environment
	ENV_DEVELOPMENT = "development"
	ENV_STAGING     = "staging"
	ENV_PRODUCTION  = "production"
)

var ErrConnectivity = errors.New("Please try again after sometime.")
