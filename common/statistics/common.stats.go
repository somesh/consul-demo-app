package statistics

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	NumberOfConfigHit = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "consul_demo",
		Name:      "number_of_config_hits",
		Help:      "Consul Demo /config API request count",
	})

// Add others here
)

func init() {

	// Service Statistics
	prometheus.MustRegister(NumberOfConfigHit)
}
