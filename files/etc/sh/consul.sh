#!/bin/sh

consul agent -config-dir=/etc/consul.d/server -config-dir=/etc/consul.d/client -log-file="/var/log/consul.log" -ui -bootstrap=true -client=0.0.0.0 >> /var/log/consul.log &
launchctl stop go.consul-demo.plist
/usr/local/bin/consul-template  --config=/var/consul/consul/config/service.consul-demo.config >> /var/log/consul-template.log &

ttab -w -s green 'tail -f /var/log/consul.log'
ttab -w -s yellow 'tail -f /usr/local/var/log/consul-demo.access.log /usr/local/var/log/consul-demo.error.log'
ttab -w -s gray 'tail -f /var/log/consul-template.log'

sleep 1
open -a "Google Chrome" --fresh http://demo.service.local/v1/api/config/list
sleep 2.5
open -a "Google Chrome" --fresh http://consul.service.local/ui/dc1/services
