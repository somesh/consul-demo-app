server {
  listen 80;
  server_name  demo.service.consul;

  set $appnode http://127.0.0.1:9000;

  proxy_set_header    X-Forwarded-For             $remote_addr;
  proxy_set_header    Host                        $host;
  proxy_set_header    X-Forwarded-Host            $host:$server_port;
  proxy_set_header    X-Forwarded-Server          $server_name;
  proxy_set_header    X-Forwarded-For             $remote_addr;
  proxy_set_header    X-Forwarded-Request-Uri     $request_uri;


  access_log /var/log/nginx/consul-demo.access.log;
  error_log  /var/log/nginx/consul-demo.error.log;


  location / {
     try_files $uri $uri/index.html @proxy;
  }

  location @proxy {
      proxy_pass   $appnode;
  }
}
