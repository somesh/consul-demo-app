package lib

import (
	"context"
	"net"
	"net/http"
	"strings"
)

const (
	ipAddressKey int = iota
	userAgentKey
	accessTokenKey
	queryParams
	version
)

func ParseRequestContext(req *http.Request) context.Context {
	ctx := req.Context()

	ctx = parseChannelVersion(ctx, req)
	ctx = parseIPAddress(ctx, req)
	ctx = parseUserAgent(ctx, req)
	ctx = parseAccessToken(ctx, req)
	ctx = parseQueryParams(ctx, req)
	return ctx
}

func GetIPAddress(ctx context.Context) string {
	val, _ := ctx.Value(ipAddressKey).(net.IP)
	return val.String()
}

func GetUserAgent(ctx context.Context) string {
	val, _ := ctx.Value(userAgentKey).(string)
	return val
}

func GetAccessToken(ctx context.Context) string {
	val, _ := ctx.Value(accessTokenKey).(string)
	return val
}

func GetQueryParams(ctx context.Context) string {
	return ctx.Value(queryParams).(string)
}

func GetChannelVersion(ctx context.Context) string {
	return ctx.Value(version).(string)
}

func parseIPAddress(ctx context.Context, req *http.Request) context.Context {
	ip, _, _ := net.SplitHostPort(req.RemoteAddr)
	if ip == "" {
		ip = "localhost"
	}
	return context.WithValue(ctx, ipAddressKey, net.ParseIP(ip))
}

func parseUserAgent(ctx context.Context, req *http.Request) context.Context {
	userAgent := req.UserAgent()
	return context.WithValue(ctx, userAgentKey, userAgent)
}

func parseAccessToken(ctx context.Context, req *http.Request) context.Context {
	var token string
	auth := strings.Split(req.Header.Get("Authorization"), "Bearer ")
	if len(auth) > 1 {
		token = auth[1]
	}
	return context.WithValue(ctx, accessTokenKey, token)
}

func parseQueryParams(ctx context.Context, req *http.Request) context.Context {
	return context.WithValue(ctx, queryParams, req.URL.RawQuery)
}

func parseChannelVersion(ctx context.Context, req *http.Request) context.Context {
	deviceVersion := req.Header.Get("X-Device")

	return context.WithValue(ctx, version, deviceVersion)
}
