package handler

import (
	"context"
	"log"
	"net/http"
	"time"
)

func (m *Module) Health(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	log.Println("doing healthCheck..")
	ctx := r.Context()
	id := r.FormValue("id")
	return m.healthCheck(ctx, id)

}

func (m *Module) healthCheck(ctx context.Context, id string) (interface{}, error) {
	start := time.Now()

	logTime(id, "service", start)

	return "OK", nil
}

func logTime(id, metrics string, start time.Time) {
	if id != "" {
		log.Printf("[%s] %s took %s\n", id, metrics, time.Since(start))
	}
}
