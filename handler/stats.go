package handler

import (
	"expvar"
	"os"

	"github.com/prometheus/client_golang/prometheus"
)

type Stats struct {
	reqs  *expvar.Int
	errs  *expvar.Int
	ipreq *expvar.Int
	perr  *prometheus.CounterVec // total errors
}

var stats Stats
var environ string

func (m *Module) initStats() {
	stats = Stats{
		reqs:  expvar.NewInt("rpsReq"), // requests per second
		errs:  expvar.NewInt("rpsErr"), // error requests per second
		ipreq: expvar.NewInt("ipReq"),
		perr: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "http_errors",
				Help: "http error responses",
			}, []string{"env"},
		),
	}

	if environ = os.Getenv("ENV"); environ == "" {
		environ = "development"
	}

	prometheus.MustRegister(stats.perr)

}
