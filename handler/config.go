package handler

import (
	"net/http"

	"github.com/opentracing/opentracing-go"

	_ "gopkg.in/tokopedia/logging.v1"

	"bitbucket.org/somesh/consul-demo-app/lib"
)

func (m *Module) ListConfig(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	ctx := r.Context()

	span := opentracing.SpanFromContext(ctx)
	defer span.Finish()

	ctx = lib.ParseRequestContext(r)

	return m.apiMod.ListConfig(ctx)
}
