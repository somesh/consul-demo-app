package handler

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func (m *Module) Init() {

	m.initStats()

	r := mux.NewRouter()

	r.Handle("/v1/api/config/list", HandlerFunc(m.ListConfig))

	// Service Health check
	r.Handle("/health", HandlerFunc(m.Health))

	http.Handle("/metrics", promhttp.Handler())

	http.Handle("/", r)
}
