package handler

import (
	"fmt"
	"log"
	"net/http"
	"time"

	consul "github.com/hashicorp/consul/api"

	"bitbucket.org/somesh/consul-demo-app/api"
	"bitbucket.org/somesh/consul-demo-app/common/config"
)

type Module struct {
	cfg *config.Config

	client       *http.Client
	apiMod       *api.Module
	consulClient *consul.Client
	log          *log.Logger
}

func Configure(cfg *config.Config) *Module {

	var err error

	m := Module{
		client: newHttpClient(cfg),
		cfg:    cfg,
		log:    config.GetLogger(),
		apiMod: api.Init(cfg),
	}

	// Setup consul client
	m.consulClient, err = consul.NewClient(&consul.Config{
		HttpClient: newHttpClient(cfg),
		Address:    fmt.Sprintf("%s:%d", cfg.Consul.Address, cfg.Consul.Port),
		Token:      cfg.Consul.Token,
	})
	if err != nil {
		m.log.Println(fmt.Sprintf("[Handler][Configure][Error][Critical] Unable to initialize consul client %s", err.Error()))
	}

	return &m
}

func newHttpClient(cfg *config.Config) *http.Client {
	timeout := cfg.Server.Timeout
	if timeout == 0 {
		timeout = 5
	}

	client := &http.Client{
		Timeout: time.Duration(timeout * time.Second), // Remaining context time = 25s
	}

	return client
}
