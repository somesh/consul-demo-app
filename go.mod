module bitbucket.org/somesh/consul-demo-app

go 1.12

require (
	github.com/eapache/go-resiliency v1.2.0
	github.com/google/gops v0.3.6
	github.com/gorilla/mux v1.7.3
	github.com/hashicorp/consul/api v1.1.0
	github.com/opentracing/basictracer-go v1.0.0 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/prometheus/client_golang v1.0.0
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/tokopedia/grace v0.0.0-20180202114526-37b319b9b800
	github.com/uber/jaeger-client-go v2.16.0+incompatible // indirect
	github.com/uber/jaeger-lib v2.0.0+incompatible // indirect
	github.com/valyala/fasthttp v1.4.0 // indirect
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/tokopedia/logging.v1 v1.0.0-20190123111544-ed577bc368d9
	gopkg.in/tylerb/graceful.v1 v1.2.15 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190107175209-d9ea5c54f7dc // indirect
	sourcegraph.com/sourcegraph/appdash-data v0.0.0-20151005221446-73f23eafcf67 // indirect
)
