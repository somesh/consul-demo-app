*Requirements*

- consul



*Dependency*
go mod init


socketmaster.
This has to be deployed as executable binary in /usr/local/bin
https://github.com/qzaidi/socketmaster


consul-template https://releases.hashicorp.com/consul-template/0.20.0/
to be templatized in ansible using https://gist.github.com/lalyos/3af51fb6520601241b10

Consul agent must run on each node that want to be part of the cluster

To test on local:

ln -s /var/consul/consul $PWD/files/var/consul

