package api

import (
	"context"

	"bitbucket.org/somesh/consul-demo-app/common/config"

	"github.com/opentracing/opentracing-go"
	_ "gopkg.in/tokopedia/logging.v1"

	"bitbucket.org/somesh/consul-demo-app/common/statistics"
)

func (api *Module) ListConfig(ctx context.Context) (*config.Config, error) {

	var span opentracing.Span

	if span = opentracing.SpanFromContext(ctx); span != nil {
		span, ctx = opentracing.StartSpanFromContext(ctx, "ListConfig")
		defer span.Finish()
	}
	statistics.NumberOfConfigHit.Inc()

	return api.cfg, nil
}
