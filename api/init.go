package api

import (
	"log"
	"net/http"
	"time"

	_ "gopkg.in/tokopedia/logging.v1"

	"bitbucket.org/somesh/consul-demo-app/common/config"
)

type Module struct {
	cfg *config.Config

	log *log.Logger

	client *http.Client
}

// Init initializes the Module & returns it.
func Init(cfg *config.Config) *Module {

	m := &Module{
		client: newHttpClient(cfg),
		cfg:    cfg,
		log:    config.GetLogger(),
	}

	return m
}

// newHttpClient creates a new HTTP client & returns it.
func newHttpClient(cfg *config.Config) *http.Client {
	//SS:TODO take timeout duration from config
	client := &http.Client{
		Timeout: time.Duration(5 * time.Second), // Remaining context time = 25s
	}

	return client
}
