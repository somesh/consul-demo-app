// This is main package. The app starts from this file.

package main

import (
	"flag"
	"log"
	"os"

	"github.com/google/gops/agent"
	"github.com/tokopedia/grace"
	"gopkg.in/tokopedia/logging.v1"

	// has init
	"bitbucket.org/somesh/consul-demo-app/common/config"

	"bitbucket.org/somesh/consul-demo-app/handler"
)

func main() {

	var configtest = flag.Bool("test", false, "config test")

	flag.Parse()
	logging.LogInit()

	// Message will not appear unless run with --debug switch
	logging.Debug.Println("app started")

	// gops helps us get stack trace if something wrong/slow in production
	opts := agent.Options{
		ShutdownCleanup: true,
	}

	if err := agent.Listen(opts); err != nil {
		log.Fatal(err)
	}

	// Get configuration for the app.
	cfg := config.GetConfig()

	// Initialise the handler
	handlers := handler.Configure(cfg)
	handlers.Init()

	if *configtest {
		os.Exit(0)
	}

	log.Fatal(grace.ServeWithConfig(":9000", cfg.Grace.ToGraceConfig(), nil))
}
